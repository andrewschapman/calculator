# Calculator V2 Implementation by Andrew Chapman

This project implements the V2 calculator as per the task specification.

Supported operations are: Alien, Skull, Ghost, Scream.

## Running the project
To run the interface, please start the native PHP web server and then open

http://localhost

If you wish, you can manually invoke the operations using GET urls like this:

Alien:
http://localhost/index.php/calculator/U+1F47D/5/6

Skull:
http://localhost/index.php/calculator/U+1F480/AA/BB

Ghost:
http://localhost/index.php/calculator/U+1F47B/7.5/3.6

Scream:
http://localhost/index.php/calculator/U+1F631/5/10

Please note that PHP V7 required.

## Unit and Functional Tests

I've coded both Unit and Functional tests using PHPUnit.  

From the project's root directory, you can run them like so:

php .\vendor\phpunit\phpunit\phpunit tests/Unit
php .\vendor\phpunit\phpunit\phpunit tests/Functional

The tests cover all 4 calculators, including expected parameters and also not expected parameters :-)

## Solution Notes

I've used SLIM V3 for the API framework, as it's fast and easy to work with, and 
comes with Composer, PS4 autoloading and PHPUnit out of the box - all very handy.

My code should be adhering to PSR2 standards.

In the spirit of the project, I wanted to make it very easy to:

- Add new calculators;
- Change the logic of existing ones without impacting the consumer code;
- Make the code as robust as possible;
- Make as many of the objects as "decoupled" as possible via interfaces;
- Have as little code duplication as possible by using abstract classes;

As such:

- Calculators are fully decoupled via the CalulatorInterface;
- Though all calculators currently require exactly 2 parameters, future calculators may need 1:n parameters.  I have catered for this flexibility by passing a "ParameterContainer" to the calculators rather than separate variables in the method prototype;
- A ParameterContainer may hold as many parameters as you like, and of varying types;
- Parameter objects have a "type" and are responsible for validating themselves.  They are also fully decoupled via the ParameterInterface (i.e. consumers don't care what the object they receive is as long as it implements the interface)
- Calculators are not responsible for the type or value of parameters they receive;
- A "CalculatorFactory" has been built that is responsible for constructing calculators and setting up the parameter container - this keeps the logic of the individual calculators simple and clear. 

## Adding Calculators

Adding new calculators is very easy.  The steps required are:

- Create a new calculator object that extends the abstract class BaseCalculator; 
- Implement the constructor method and the  "calculate" method as per the CalculatorInterface, and return the calculated value;
- Add support for instantiating the new Calculator object in the CalculatorFactory.  Note you will need to add a new Unicode type: e.g. const TYPE_SCREAM = 'U+1F631';

Swapping out Calculators for others is also easy and will work seamlessly as long as the interface is implemented.

## Closing notes

I did consider adding a further object, a "CalculatorService", that would have 
been in charge of executing the "calculate" method of the Calculators. In this way 
consumer code would instantiate a CalculatorService object, and then invoke the
calculators via the service.

By adding an additional layer, we could add additional features such as:
- Logging all calls, parameters and results to/from Calculators;
- Implementing a caching layer so any previously calculated values could be returned without recalculating;

In  the end I felt this was probably out of scope, but I thought I'd mention it.

Thanks again,
Cheers,
Andrew.


