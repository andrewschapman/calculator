<?php

namespace Tests\Functional;

use CalculatorDemo\Calculators\CalculatorFactory;

class CalculatorTest extends BaseTestCase
{
    /**
     * Test running the Alien calculator via Slim.
     */
    public function testAlienCalculator()
    {
        $url = '/calculator/' . CalculatorFactory::TYPE_ALIEN . '/5/10';

        $response = $this->runApp('GET', $url);

        // Ensure we received a 200 OK
        $this->assertEquals(200, $response->getStatusCode());

        // Payload must not be empty
        $payload = (string)$response->getBody();
        $this->assertFalse(empty($payload));

        // Must be able to json_decode the contents
        $data = @json_decode($payload);
        $this->assertTrue(is_object($data));

        // The result property must exist
        $this->assertTrue(property_exists($data, 'result'));

        // Result should be 50
        $this->assertTrue($data->result == 50);
    }

    /**
     * Test running the Skull calculator via Slim.
     */
    public function testSkullCalculator()
    {
        $url = '/calculator/' . CalculatorFactory::TYPE_SKULL . '/AA/BB';

        $response = $this->runApp('GET', $url);

        // Ensure we received a 200 OK
        $this->assertEquals(200, $response->getStatusCode());

        // Payload must not be empty
        $payload = (string)$response->getBody();
        $this->assertFalse(empty($payload));

        // Must be able to json_decode the contents
        $data = @json_decode($payload);
        $this->assertTrue(is_object($data));

        // The result property must exist
        $this->assertTrue(property_exists($data, 'result'));

        // Verify result
        $this->assertTrue($data->result == 'AA : BB');
    }

    /**
     * Test running the Ghost calculator via Slim.
     */
    public function testGhostCalculator()
    {
        $url = '/calculator/' . CalculatorFactory::TYPE_GHOST . '/8/10';

        $response = $this->runApp('GET', $url);

        // Ensure we received a 200 OK
        $this->assertEquals(200, $response->getStatusCode());

        // Payload must not be empty
        $payload = (string)$response->getBody();
        $this->assertFalse(empty($payload));

        // Must be able to json_decode the contents
        $data = @json_decode($payload);
        $this->assertTrue(is_object($data));

        // The result property must exist
        $this->assertTrue(property_exists($data, 'result'));

        // Verify result
        $this->assertTrue($data->result == 21);
    }

    /**
     * Test running the Scream calculator via Slim.
     */
    public function testScreamCalculator()
    {
        $url = '/calculator/' . CalculatorFactory::TYPE_SCREAM . '/150/10';

        $response = $this->runApp('GET', $url);

        // Ensure we received a 200 OK
        $this->assertEquals(200, $response->getStatusCode());

        // Payload must not be empty
        $payload = (string)$response->getBody();
        $this->assertFalse(empty($payload));

        // Must be able to json_decode the contents
        $data = @json_decode($payload);
        $this->assertTrue(is_object($data));

        // The result property must exist
        $this->assertTrue(property_exists($data, 'result'));

        // Verify result
        $this->assertTrue($data->result == 'U+1F383,U+1F47E,U+1F47E,U+1F383,U+1F47E,U+1F383,U+1F383,U+1F47E' .
            ' ' .
            'U+1F383,U+1F47E,U+1F383,U+1F47E');
    }
}