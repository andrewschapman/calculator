<?php
/**
 * Created by PhpStorm.
 * User: Andrew Chapman
 */
declare(strict_types=1);

use PHPUnit\Framework\TestCase;
use CalculatorDemo\Calculators\CalculatorFactory;
use CalculatorDemo\Parameters\ParameterFactory;
use CalculatorDemo\Calculators\Exceptions\InvalidParameterCountException;
use CalculatorDemo\Parameters\Exceptions\InvalidParameterTypeException;
use CalculatorDemo\Parameters\Exceptions\InvalidParameterValueException;

final class CalculatorTest extends TestCase
{
    public function testAlienCalculator():void
    {
        // Ensure simple multiplication works
        $calculator = CalculatorFactory::createCalculator(CalculatorFactory::TYPE_ALIEN, [10, 5]);
        $result = $calculator->calculate();
        $this->assertTrue($result == 50);

        // Ensure negative numbers work
        $calculator = CalculatorFactory::createCalculator(CalculatorFactory::TYPE_ALIEN, [-10, 5]);
        $result = $calculator->calculate();
        $this->assertTrue($result == -50);

        $calculator = CalculatorFactory::createCalculator(CalculatorFactory::TYPE_ALIEN, [10, -5]);
        $result = $calculator->calculate();
        $this->assertTrue($result == -50);

        $calculator = CalculatorFactory::createCalculator(CalculatorFactory::TYPE_ALIEN, [-10, -5]);
        $result = $calculator->calculate();
        $this->assertTrue($result == 50);

        // Ensure zero multiplication yields 0
        $calculator = CalculatorFactory::createCalculator(CalculatorFactory::TYPE_ALIEN, [1, 0]);
        $result = $calculator->calculate();
        $this->assertTrue($result == 0);

        // Ensure floats are cast as integers
        $calculator = CalculatorFactory::createCalculator(CalculatorFactory::TYPE_ALIEN, [5.5, 7.5]);
        $result = $calculator->calculate();
        $this->assertTrue($result == 35);

        // Test invalid parameter inputs - these tests are expected to throw InvalidParameterTypeException exceptions.
        // If they don't, then something has gone wrong.
        $ok = false;

        try {
            CalculatorFactory::createCalculator(CalculatorFactory::TYPE_ALIEN, ['Hello', 5]);
        } catch (InvalidParameterTypeException $e) {
            $ok = true;
        } catch (InvalidParameterValueException $e) {
            $ok = true;
        }

        if (!$ok) {
            $this->fail('An exception was not thrown - invalid parameters allowed');
        }

        $ok = false;

        try {
            CalculatorFactory::createCalculator(CalculatorFactory::TYPE_ALIEN, [3, 'Hello']);
        } catch (InvalidParameterTypeException $e) {
            $ok = true;
        } catch (InvalidParameterValueException $e) {
            $ok = true;
        }

        if (!$ok) {
            $this->fail('An exception was not thrown - invalid parameters allowed');
        }

        $ok = false;

        try {
            CalculatorFactory::createCalculator(CalculatorFactory::TYPE_ALIEN, ['', 5]);
        } catch (InvalidParameterTypeException $e) {
            $ok = true;
        } catch (InvalidParameterValueException $e) {
            $ok = true;
        }

        if (!$ok) {
            $this->fail('An exception was not thrown - invalid parameters allowed');
        }

        $ok = false;

        try {
            CalculatorFactory::createCalculator(CalculatorFactory::TYPE_ALIEN, [5, '']);
        } catch (InvalidParameterTypeException $e) {
            $ok = true;
        } catch (InvalidParameterValueException $e) {
            $ok = true;
        }

        if (!$ok) {
            $this->fail('An exception was not thrown - invalid parameters allowed');
        }

        $ok = false;

        try {
            CalculatorFactory::createCalculator(CalculatorFactory::TYPE_ALIEN, [' ', ' ']);
        } catch (InvalidParameterTypeException $e) {
            $ok = true;
        } catch (InvalidParameterValueException $e) {
            $ok = true;
        }

        if (!$ok) {
            $this->fail('An exception was not thrown - invalid parameters allowed');
        }

        // Test invalid number of parameter inputs
        $this->checkInvalidNumberOfParams(CalculatorFactory::TYPE_ALIEN, 2, ParameterFactory::PARAMETER_TYPE_INT);
    }

    public function testSkullCalculator():void
    {
        // Test concatenation of different variable types
        $calculator = CalculatorFactory::createCalculator(CalculatorFactory::TYPE_SKULL, ['HELLO', 'WORLD']);
        $result = $calculator->calculate();
        $this->assertTrue($result == 'HELLO : WORLD');

        $calculator = CalculatorFactory::createCalculator(CalculatorFactory::TYPE_SKULL, [5, 'WORLD']);
        $result = $calculator->calculate();
        $this->assertTrue($result == '5 : WORLD');

        $calculator = CalculatorFactory::createCalculator(CalculatorFactory::TYPE_SKULL, ['HELLO', 10]);
        $result = $calculator->calculate();
        $this->assertTrue($result == 'HELLO : 10');

        $calculator = CalculatorFactory::createCalculator(CalculatorFactory::TYPE_SKULL, [5, 10]);
        $result = $calculator->calculate();
        $this->assertTrue($result == '5 : 10');

        // Test invalid parameters
        try {
            CalculatorFactory::createCalculator(CalculatorFactory::TYPE_SKULL, ['', 'WORLD']);
            $this->fail('An exception was not thrown - invalid parameters allowed');
        } catch (InvalidParameterTypeException $e) {
            $ok = true;
        } catch (InvalidParameterValueException $e) {
            $ok = true;
        }

        try {
            CalculatorFactory::createCalculator(CalculatorFactory::TYPE_SKULL, ['HELLO', '']);
            $this->fail('An exception was not thrown - invalid parameters allowed');
        } catch (InvalidParameterTypeException $e) {
            $ok = true;
        } catch (InvalidParameterValueException $e) {
            $ok = true;
        }

        // Test invalid number of parameter inputs
        $this->checkInvalidNumberOfParams(CalculatorFactory::TYPE_SKULL, 2, ParameterFactory::PARAMETER_TYPE_STRING);
    }

    public function testGhostCalculator():void
    {
        // Test standard ghost calculation
        $calculator = CalculatorFactory::createCalculator(CalculatorFactory::TYPE_GHOST, [8, 10]);
        $result = $calculator->calculate();
        $this->assertTrue($result == 21);

        // Test floating point ghost calculation
        $calculator = CalculatorFactory::createCalculator(CalculatorFactory::TYPE_GHOST, [7.5, 8.5]);
        $result = $calculator->calculate();
        $this->assertTrue($result == 19.3);

        // Ensure result has only 1 decimal place
        $resultStr = (string)$result;
        $dotPos = strpos($resultStr, '.');
        $this->assertTrue(strlen($resultStr) == ($dotPos + 2));

        // Test invalid parameter inputs
        $ok = false;

        try {
            CalculatorFactory::createCalculator(CalculatorFactory::TYPE_GHOST, ['Hello', 5]);
        } catch (InvalidParameterTypeException $e) {
            $ok = true;
        } catch (InvalidParameterValueException $e) {
            $ok = true;
        }

        if (!$ok) {
            $this->fail('An exception was not thrown - invalid parameters allowed');
        }

        $ok = false;

        try {
            CalculatorFactory::createCalculator(CalculatorFactory::TYPE_GHOST, [10, 'World']);
        } catch (InvalidParameterTypeException $e) {
            $ok = true;
        } catch (InvalidParameterValueException $e) {
            $ok = true;
        }

        if (!$ok) {
            $this->fail('An exception was not thrown - invalid parameters allowed');
        }

        // Test invalid number of parameter inputs
        $this->checkInvalidNumberOfParams(CalculatorFactory::TYPE_GHOST, 2, ParameterFactory::PARAMETER_TYPE_FLOAT);
    }

    public function testScreamCalculator():void
    {
        // Test standard scream calculation
        $calculator = CalculatorFactory::createCalculator(CalculatorFactory::TYPE_SCREAM, [150, 10]);
        $result = $calculator->calculate();
        $this->assertTrue($result == 'U+1F383,U+1F47E,U+1F47E,U+1F383,U+1F47E,U+1F383,U+1F383,U+1F47E' .
            ' ' .
            'U+1F383,U+1F47E,U+1F383,U+1F47E');

        $calculator = CalculatorFactory::createCalculator(CalculatorFactory::TYPE_SCREAM, [3, 10]);
        $result = $calculator->calculate();
        $this->assertTrue($result == 'U+1F383,U+1F383' .
            ' ' .
            'U+1F383,U+1F47E,U+1F383,U+1F47E');

        // Test maximum range values
        $calculator = CalculatorFactory::createCalculator(CalculatorFactory::TYPE_SCREAM, [0, 255]);
        $result = $calculator->calculate();
        $this->assertTrue($result == 'U+1F47E' .
            ' ' .
            'U+1F383,U+1F383,U+1F383,U+1F383,U+1F383,U+1F383,U+1F383,U+1F383');

        // Test out of range calculation
        $calculator = CalculatorFactory::createCalculator(CalculatorFactory::TYPE_SCREAM, [-1, 256]);
        $result = $calculator->calculate();
        $this->assertTrue($result == 'U+1F4A9' .
            ' ' .
            'U+1F4A9');

        // Test invalid parameter inputs - these tests are expected to throw InvalidParameterTypeException exceptions.
        // If they don't, then something has gone wrong.
        $ok = false;

        try {
            CalculatorFactory::createCalculator(CalculatorFactory::TYPE_SCREAM, ['Hello', 5]);
        } catch (InvalidParameterTypeException $e) {
            $ok = true;
        } catch (InvalidParameterValueException $e) {
            $ok = true;
        }

        if (!$ok) {
            $this->fail('An exception was not thrown - invalid parameters allowed');
        }

        $ok = false;

        try {
            CalculatorFactory::createCalculator(CalculatorFactory::TYPE_SCREAM, [100, 'World']);
        } catch (InvalidParameterTypeException $e) {
            $ok = true;
        } catch (InvalidParameterValueException $e) {
            $ok = true;
        }

        if (!$ok) {
            $this->fail('An exception was not thrown - invalid parameters allowed');
        }

        // Test invalid number of parameter inputs
        $this->checkInvalidNumberOfParams(
            CalculatorFactory::TYPE_SCREAM,
            2,
            ParameterFactory::PARAMETER_TYPE_INT
        );
    }

    /**
     * Ensures that the given calculator only accepts the required number of parameters.
     * E.g. if the calculator requires 2 parameters, 0, 1 and 3 inputs are NOT allowed.
     * @param string $calculatorType The type of calculator to test
     * @param int $correctNumberOfParams The correct number of parameters for this function
     * @param int $paramType The type of parameters expected (INT, FLOAT, STRING)
     */
    private function checkInvalidNumberOfParams(
        string $calculatorType,
        int $correctNumberOfParams,
        int $paramType
    ):void {
        // Loop through from 0 parameters to number of parameters + 1
        for ($currentNumParams = 0; $currentNumParams <= ($correctNumberOfParams + 1); $currentNumParams++) {
            // Do not test the correct number of parameters
            if ($currentNumParams == $correctNumberOfParams) {
                continue;
            }

            // Build a params array with the number of params we want to test and of the correct type.
            $params = [];

            if ($currentNumParams > 0) {
                for ($paramNo = 1; $paramNo <= $currentNumParams; $paramNo++) {
                    switch ($paramType) {
                        case ParameterFactory::PARAMETER_TYPE_INT:
                            $params[] = 1;
                            break;

                        case ParameterFactory::PARAMETER_TYPE_FLOAT:
                            $params[] = 1.5;
                            break;

                        case ParameterFactory::PARAMETER_TYPE_STRING:
                            $params[] = 'HELLO';
                            break;
                    }
                }
            }

            // Run the test - it should throw an InvalidParameterCountException exception.
            $ok = false;

            try {
                CalculatorFactory::createCalculator($calculatorType, $params);
            } catch (InvalidParameterCountException $e) {
                // Exception was thrown, all OK
                $ok = true;
            }

            // If no exception was thrown, fail the test.
            if (!$ok) {
                $this->fail('An exception was not thrown - invalid number of parameters allowed');
            }
        }
    }
}
