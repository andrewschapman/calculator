<?php
// Routes

$app->get('/calculator/{operator}/{parameter1}/{parameter2}', CalculatorHandler::class . ':calculate');

$app->get('/', function ($request, $response, $args) {
    return $this->renderer->render($response, 'index.phtml', [
        //'name' => $args['name']
    ]);
})->setName('home');
