<?php
/**
 * Created by PhpStorm.
 * User: Andrew Chapman
 */

namespace CalculatorDemo\Parameters;

use CalculatorDemo\Parameters\Exceptions\InvalidParameterValueException;

final class ParameterInt extends AbstractParameter
{
    public function __construct($value)
    {
        parent::__construct('INT', $value);
    }

    /**
     * Validates that the passed value can be converted to a valid integer
     * and returns it as an integer.  Will be called by the parent class
     * constructor.
     * @param string $value
     * @return int
     * @throws InvalidParameterValueException
     */
    protected function validate(string $value):int
    {
        // Ensure the value passed can be converted to a number.
        if (!is_numeric($value)) {
            throw new InvalidParameterValueException($this->getType(), $value);
        }

        // Cast the value to an integer and return it.
        return intval($value);
    }
}