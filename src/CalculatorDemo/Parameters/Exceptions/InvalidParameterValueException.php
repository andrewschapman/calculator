<?php
/**
 * Created by PhpStorm.
 * User: Andrew Chapman
 */

namespace CalculatorDemo\Parameters\Exceptions;

use \Exception;

class InvalidParameterValueException extends Exception
{
    public function __construct(string $requiredType, string $actualValue)
    {
        $message = sprintf(
            'Parameter must be a valid %s.  Value given was %s.',
            $requiredType,
            $actualValue
        );

        parent::__construct($message, 3, null);
    }

    public function __toString()
    {
        return __CLASS__ . ": [{$this->code}]: {$this->message}\n";
    }
}