<?php
/**
 * Created by PhpStorm.
 * User: Andrew
 */

namespace CalculatorDemo\Parameters\Exceptions;

use \Exception;

class InvalidParameterTypeException extends Exception
{
    public function __construct(string $parameterType)
    {
        parent::__construct('Invalid Parameter Type: ' . $parameterType, 2, null);
    }

    public function __toString()
    {
        return __CLASS__ . ": [{$this->code}]: {$this->message}\n";
    }
}