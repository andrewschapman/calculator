<?php
/**
 * Created by PhpStorm.
 * User: Andrew Chapman
 */

namespace CalculatorDemo\Parameters\Exceptions;

use \Exception;

class InvalidParameterNumberException extends Exception
{
    public function __construct(int $parameterNumber)
    {
        parent::__construct('Invalid Parameter number: ' . $parameterNumber, 3, null);
    }

    public function __toString()
    {
        return __CLASS__ . ": [{$this->code}]: {$this->message}\n";
    }
}