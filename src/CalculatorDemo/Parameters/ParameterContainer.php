<?php
/**
 * Created by PhpStorm.
 * User: Andrew Chapman
 */

namespace CalculatorDemo\Parameters;

use CalculatorDemo\Parameters\Exceptions\InvalidParameterNumberException;
use CalculatorDemo\Parameters\Interfaces\ParameterInterface;
use CalculatorDemo\Parameters\Interfaces\ParameterContainerInterface;

final class ParameterContainer implements ParameterContainerInterface
{
    /**
     * @var int
     */
    private $numParameters;

    /**
     * @var array
     */
    private $parameters;

    public function __construct()
    {
        $this->numParameters = 0;
        $this->parameters = [];
    }

    /**
     * @param int $parameterType
     * @param $value
     * @throws Exceptions\InvalidParameterTypeException
     * @throws Exceptions\InvalidParameterValueException
     */
    public function addParameter(int $parameterType, $value)
    {
        $this->parameters[] = ParameterFactory::createParameter($parameterType, $value);
        $this->numParameters++;
    }

    /**
     * Returns a parameter from the container as specified by the parameter number.
     * @param int $parameterNumber
     * @return ParameterInterface
     * @throws InvalidParameterNumberException
     */
    public function getParameter(int $parameterNumber): ParameterInterface
    {
        if (($parameterNumber < 1) || ($parameterNumber > $this->numParameters)) {
            throw new InvalidParameterNumberException($parameterNumber);
        }

        return $this->parameters[$parameterNumber - 1];
    }
}