<?php
/**
 * Created by PhpStorm.
 * User: Andrew Chapman
 */

namespace CalculatorDemo\Parameters;

use CalculatorDemo\Parameters\Exceptions\InvalidParameterValueException;

final class ParameterFloat extends AbstractParameter
{
    public function __construct($value)
    {
        parent::__construct('FLOAT', $value);
    }

    /**
     * Validates that the passed value can be converted to a valid float
     * and returns it as a float.  Will be called by the parent class
     * constructor.
     * @param string $value
     * @return float
     * @throws InvalidParameterValueException
     */
    protected function validate(string $value):float
    {
        // Ensure the value passed can be converted to a number.
        if (!is_numeric($value)) {
            throw new InvalidParameterValueException($this->getType(), $value);
        }

        // Cast the value to a float and return it.
        return floatval($value);
    }
}