<?php
/**
 * Created by PhpStorm.
 * User: Andrew Chapman
 */

namespace CalculatorDemo\Parameters;

use CalculatorDemo\Parameters\Exceptions\InvalidParameterTypeException;
use CalculatorDemo\Parameters\Interfaces\ParameterInterface;

final class ParameterFactory
{
    const PARAMETER_TYPE_INT = 1;
    const PARAMETER_TYPE_STRING = 2;
    const PARAMETER_TYPE_FLOAT = 3;

    /**
     * Creates a new parameter object of the specified type
     * @param int $parameterType
     * @param $value
     * @return ParameterInterface
     * @throws InvalidParameterTypeException
     */
    public static function createParameter(int $parameterType, $value)
    {
        switch ($parameterType) {
            case self::PARAMETER_TYPE_INT:
                return self::createParameterInt($value);
                break;

            case self::PARAMETER_TYPE_STRING:
                return self::createParameterString($value);
                break;

            case self::PARAMETER_TYPE_FLOAT:
                return self::createParameterFloat($value);
                break;

            default:
                throw new InvalidParameterTypeException($parameterType);
                break;
        }
    }

    /**
     * Creates a new integer parameter
     * @param $value
     * @return ParameterInterface
     */
    public static function createParameterInt($value):ParameterInterface
    {
        return new ParameterInt($value);
    }

    /**
     * Creates a new string parameter
     * @param $value
     * @return ParameterInterface
     */
    public static function createParameterString($value):ParameterInterface
    {
        return new ParameterString($value);
    }

    /**
     * Creates a new float parameter
     * @param $value
     * @return ParameterInterface
     */
    public static function createParameterFloat($value):ParameterInterface
    {
        return new ParameterFloat($value);
    }
}