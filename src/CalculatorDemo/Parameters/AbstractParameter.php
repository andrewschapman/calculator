<?php
/**
 * Created by PhpStorm.
 * User: Andrew Chapman
 */

namespace CalculatorDemo\Parameters;

class AbstractParameter implements Interfaces\ParameterInterface
{
    private $value;
    protected $type;

    public function __construct(string $type, string $value)
    {
        $this->type = $type;
        $this->value = $this->validate($value);
    }

    protected function setValue($value)
    {
        $this->value = $value;
    }

    public function getValue()
    {
        return $this->value;
    }

    public function getType():string
    {
        return $this->type;
    }

    protected function validate(string $value)
    {
        return $value;
    }
}