<?php
/**
 * Created by PhpStorm.
 * User: Andrew
 * Date: 26/02/2017
 * Time: 10:17
 */

namespace CalculatorDemo\Parameters\Interfaces;

interface ParameterContainerInterface
{
    public function getParameter(int $parameterNumber):ParameterInterface;
}