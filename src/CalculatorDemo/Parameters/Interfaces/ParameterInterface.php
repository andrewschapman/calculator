<?php
/**
 * Created by PhpStorm.
 * User: Andrew
 * Date: 26/02/2017
 * Time: 09:13
 */

namespace CalculatorDemo\Parameters\Interfaces;

interface ParameterInterface
{
    public function getValue();
    public function getType();
}