<?php
/**
 * Created by PhpStorm.
 * User: Andrew Chapman
 */

namespace CalculatorDemo\Parameters;

use CalculatorDemo\Parameters\Exceptions\InvalidParameterValueException;

final class ParameterString extends AbstractParameter
{
    public function __construct($value)
    {
        parent::__construct('STRING', $value);
    }

    /**
     * Validates that the passed value is not empty
     * @param string $value
     * @return int
     * @throws InvalidParameterValueException
     */
    protected function validate(string $value)
    {
        // Ensure the value passed is a valid string (not empty)
        if (empty($value)) {
            throw new InvalidParameterValueException($this->getType(), $value);
        }

        // Value is OK.
        return $value;
    }
}