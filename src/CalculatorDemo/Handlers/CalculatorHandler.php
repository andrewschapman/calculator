<?php
/**
 * Created by PhpStorm.
 * User: Andrew Chapman
 */

namespace CalculatorDemo\Handlers;

use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\ResponseInterface;
use Slim\Container;
use CalculatorDemo\Calculators\CalculatorFactory;

class CalculatorHandler extends BaseHandler
{
    public function __construct(Container $container)
    {
        parent::__construct($container);
    }

    public function calculate(
        ServerRequestInterface $request,
        ResponseInterface $response,
        array $args
    ) {
        // Ensure we have at least 3 parameters passed in the URL.
        if (count($args) < 3) {
            return $this->handleError($response, ERRORCODE_INVALID_PARAMETERS);
        }

        // Ensure all parameters have a value
        foreach ($args as $key => $val) {
            if ($val == '') {
                return $this->handleError($response, ERRORCODE_INVALID_PARAMETER, $key);
            }
        }

        // Read in the operator
        $operator = $args['operator'] ?? '';

        if ($operator == '') {
            return $this->handleError($response, ERRORCODE_INVALID_PARAMETER, 'operator');
        }

        // The operator, e.g. U+1F47B will lose the + symbol due to url decoding.
        // Re-encode it to put the + back.
        $operator = urlencode($operator);

        try {
            // Pass our parameters through to our calculator.
            // Slice the values from the $args array, skipping the first element (the operator).
            $calculator = CalculatorFactory::createCalculator($operator, array_values(array_slice($args, 1)));
            $result = $calculator->calculate();

            return $response->withJson(['result' => $result]);
        } catch (\Exception $e) {
            return $this->handleError($response, ERRORCODE_GENERAL_EXCEPTION, $e->getMessage());
        }
    }
}