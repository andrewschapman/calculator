<?php
/**
 * Created by PhpStorm.
 * User: Andrew Chapman
 */

namespace CalculatorDemo\Handlers;

use Psr\Http\Message\ResponseInterface;

abstract class BaseHandler
{
    protected $container;

    public function __construct(\Slim\Container $container)
    {
        $this->container = $container;
    }

    /**
     * Returns a standard error response in JSON encoding.
     * Note that error codes are stored in /errorCodes.php
     *
     * @param ResponseInterface $response
     * @param integer $errorCode
     * @param array $info
     * @return mixed
     */
    protected function handleError(ResponseInterface $response, $errorCode, $info = [])
    {
        // Get the errorCodeManager from the di container
        $errorCodeManager = $this->container->get('ErrorCodeManager');

        // Get the error response and http status code
        list($errorData, $httpStatusCode) = $errorCodeManager->getErrorCodeResponse($errorCode);

        // If additional information was passed in the information array,
        // include that in the response.
        if (count($info) > 0) {
            $errorData['information'] = $info;
        }

        return $response->withJson($errorData, $httpStatusCode);
    }
}