<?php
/**
 * Created by PhpStorm.
 * User: Andrew Chapman
 */

namespace CalculatorDemo\Calculators;

use CalculatorDemo\Parameters\Interfaces\ParameterContainerInterface;

define('UNICODE_ICON_BINARY_MAP', [
    'U+1F47E',
    'U+1F383'
]);

define('UNICODE_ICON_LARGE_NUMBER', 'U+1F4A9');

final class ScreamCalculator extends BaseCalculator
{
    public function __construct(ParameterContainerInterface $params)
    {
        parent::__construct($params);
    }

    /**
     * Receives two integer parameters via the params container.
     * For each parameter, the following logic is executed:
     *
     * For numbers from 0-255 the values are converted to binary.
     * For each 0 in the binary string, the unicode value U+1F47E is
     * appended to a string.  For each 1 in the binary string, the
     * unicode value U+1F383 is appended to the string.
     *
     * Once both parameters are converted into unicode icon strings,
     * they are appended into a single string, separated by a space
     * and returned.
     *
     * @return string
     */
    public function calculate():string
    {
        $parameter1 = $this->getParams()->getParameter(1)->getValue();
        $parameter2 = $this->getParams()->getParameter(2)->getValue();

        $convertValue = function ($value) {
            return (($value < 0) || ($value > 255)) ?
                UNICODE_ICON_LARGE_NUMBER :
                $this->makeScreamString($value);
        };

        $result = $convertValue($parameter1);
        $result .= " ";
        $result .= $convertValue($parameter2);

        return $result;
    }

    /**
     * Converts a decimal number, e.g. 10, to unicode icon string
     * where each icon represents a bit value.
     * @param $value
     * @return array
     */
    private function makeScreamString($value)
    {
        $binaryArray = $this->convertToBinary($value);

        /**
         * Walk through each of the bit values in the
         * binary string and return the unicode icon value  for it.
         */
        $unicodeArray = array_map(function ($bitValue) {
            return UNICODE_ICON_BINARY_MAP[intval($bitValue)];
        }, $binaryArray);

        return join(',', $unicodeArray);
    }

    /**
     * Converts a decimal number, e.g. 10, to a binary array,
     * e.g. [1,0,1,0]
     * @param int $value
     * @return array
     */
    private function convertToBinary(int $value)
    {
        return str_split(decbin($value));
    }
}