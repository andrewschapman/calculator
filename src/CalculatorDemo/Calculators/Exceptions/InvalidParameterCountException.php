<?php
/**
 * Created by PhpStorm.
 * User: Andrew Chapman
 */

namespace CalculatorDemo\Calculators\Exceptions;

use \Exception;

/**
 * Class InvalidParameterCountException
 * @package CalculatorDemo\Parameters\Exceptions
 */
class InvalidParameterCountException extends Exception
{
    public function __construct(int $actualParameterCount, int $neededParameterCount)
    {
        parent::__construct(
            sprintf('Invalid number of parameters provided.  ' .
                '%d needed, %d provided', $neededParameterCount, $actualParameterCount),
            3,
            null
        );
    }

    public function __toString()
    {
        return __CLASS__ . ": [{$this->code}]: {$this->message}\n";
    }
}