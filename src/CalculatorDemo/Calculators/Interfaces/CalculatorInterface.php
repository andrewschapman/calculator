<?php
/**
 * Created by PhpStorm.
 * User: Andrew Chapman
 */

namespace CalculatorDemo\Calculators\Interfaces;

use CalculatorDemo\Parameters\Interfaces\ParameterContainerInterface;

interface CalculatorInterface
{
    public function __construct(ParameterContainerInterface $params);
    public function calculate();
}