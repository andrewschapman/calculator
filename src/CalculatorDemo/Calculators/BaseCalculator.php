<?php
/**
 * Created by PhpStorm.
 * User: Andrew Chapman
 */

namespace CalculatorDemo\Calculators;

use CalculatorDemo\Parameters\Interfaces\ParameterContainerInterface;
use CalculatorDemo\Calculators\Interfaces\CalculatorInterface;

abstract class BaseCalculator implements CalculatorInterface
{
    /**
     * @var ParameterContainerInterface
     */
    private $params;

    public function __construct(ParameterContainerInterface $params)
    {
        $this->params = $params;
    }

    /**
     * Returns access to the parameter collection
     * @return ParameterContainerInterface
     */
    protected function getParams():ParameterContainerInterface
    {
        return $this->params;
    }
}