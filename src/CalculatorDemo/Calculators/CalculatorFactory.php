<?php
/**
 * Created by PhpStorm.
 * User: Andrew Chapman
 */

namespace CalculatorDemo\Calculators;

use CalculatorDemo\Calculators\Interfaces\CalculatorInterface;
use CalculatorDemo\Parameters\ParameterContainer;
use CalculatorDemo\Parameters\ParameterFactory;
use CalculatorDemo\Calculators\Exceptions\InvalidParameterCountException;

final class CalculatorFactory
{
    const TYPE_ALIEN = 'U+1F47D';
    const TYPE_SKULL = 'U+1F480';
    const TYPE_GHOST = 'U+1F47B';
    const TYPE_SCREAM = 'U+1F631';

    /**
     * Creates a new Calculator that implements the CalculatorInterface
     * @param string $calculatorType Specifies the type of calculator to create
     * @param array $params A mixed type array containing the parameters for the calculator
     * @return CalculatorInterface
     * @throws \Exception
     */
    public static function createCalculator(string $calculatorType, array $params):CalculatorInterface
    {
        switch ($calculatorType) {
            case self::TYPE_ALIEN:
                self::enforceValidParamCount($params, 2);
                return self::createAlienCalculator($params[0], $params[1]);
                break;

            case self::TYPE_SKULL:
                self::enforceValidParamCount($params, 2);
                return self::createSkullCalculator($params[0], $params[1]);
                break;

            case self::TYPE_GHOST:
                self::enforceValidParamCount($params, 2);
                return self::createGhostCalculator($params[0], $params[1]);
                break;

            case self::TYPE_SCREAM:
                self::enforceValidParamCount($params, 2);
                return self::createScreamCalculator($params[0], $params[1]);
                break;

            default:
                throw new \Exception('CalculatorFactory::createCalculator - ' .
                    'Unsupported calculator type: ' . $calculatorType);
                break;
        }
    }

    /**
     * Creates a new instance of AlienCalculator
     * @param $parameter1
     * @param $parameter2
     * @return AlienCalculator
     * @throws \Exception
     */
    public static function createAlienCalculator($parameter1, $parameter2):AlienCalculator
    {
        $parameterContainer = new ParameterContainer();

        // The Test Calculator requires 2 INT parameters
        $parameterContainer->addParameter(ParameterFactory::PARAMETER_TYPE_INT, $parameter1);
        $parameterContainer->addParameter(ParameterFactory::PARAMETER_TYPE_INT, $parameter2);

        $calculator = new AlienCalculator($parameterContainer);

        return $calculator;
    }

    /**
     * Creates a new instance of SkullCalculator
     * @param $parameter1
     * @param $parameter2
     * @return SkullCalculator
     * @throws \Exception
     */
    public static function createSkullCalculator($parameter1, $parameter2):SkullCalculator
    {
        $parameterContainer = new ParameterContainer();

        // The Test Calculator requires 2 STRING parameters

        $parameterContainer->addParameter(ParameterFactory::PARAMETER_TYPE_STRING, $parameter1);
        $parameterContainer->addParameter(ParameterFactory::PARAMETER_TYPE_STRING, $parameter2);

        $calculator = new SkullCalculator($parameterContainer);

        return $calculator;
    }

    /***
     * Creates a new instance of the Ghost calculator
     * @param mixed $parameter1 Must be convertible to a float value
     * @param mixed $parameter2 Must be convertible to a float value
     * @return GhostCalculator
     */
    public static function createGhostCalculator($parameter1, $parameter2):GhostCalculator
    {
        $parameterContainer = new ParameterContainer();

        // The Test Calculator requires 2 FLOAT parameters
        $parameterContainer->addParameter(ParameterFactory::PARAMETER_TYPE_FLOAT, $parameter1);
        $parameterContainer->addParameter(ParameterFactory::PARAMETER_TYPE_FLOAT, $parameter2);

        $calculator = new GhostCalculator($parameterContainer);

        return $calculator;
    }

    /**
     * Creates a new instance of the ScreamCalculator
     * @param $parameter1
     * @param $parameter2
     * @return ScreamCalculator
     * @throws \Exception
     */
    public static function createScreamCalculator($parameter1, $parameter2):ScreamCalculator
    {
        $parameterContainer = new ParameterContainer();

        // The Scream Calculator requires 2 INT parameters
        $parameterContainer->addParameter(ParameterFactory::PARAMETER_TYPE_INT, $parameter1);
        $parameterContainer->addParameter(ParameterFactory::PARAMETER_TYPE_INT, $parameter2);

        $calculator = new ScreamCalculator($parameterContainer);

        return $calculator;
    }

    /**
     * Ensures that the number of params passed in is correct for the calculator type.
     * @param array $params
     * @param int $requiredParamCount
     * @throws InvalidParameterCountException
     */
    private static function enforceValidParamCount(array &$params, int $requiredParamCount)
    {
        $paramCount = count($params);

        if ($paramCount != $requiredParamCount) {
            throw new InvalidParameterCountException($paramCount, $requiredParamCount);
        }
    }
}