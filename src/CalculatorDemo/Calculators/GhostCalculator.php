<?php
/**
 * Created by PhpStorm.
 * User: Andrew Chapman
 */

namespace CalculatorDemo\Calculators;

use CalculatorDemo\Parameters\Interfaces\ParameterContainerInterface;

final class GhostCalculator extends BaseCalculator
{
    public function __construct(ParameterContainerInterface $params)
    {
        parent::__construct($params);
    }

    /**
     * Double the first parameter, halves the second one, and adds the result
     * of both together.  Result is returned with 1 decimal place only.
     * @return int
     */
    public function calculate():float
    {
        return number_format(
            (($this->getParams()->getParameter(1)->getValue() * 2) +
                ($this->getParams()->getParameter(2)->getValue() / 2)),
            1
        );
    }
}