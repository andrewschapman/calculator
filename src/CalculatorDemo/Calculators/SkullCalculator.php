<?php
/**
 * Created by PhpStorm.
 * User: Andrew Chapman
 */

namespace CalculatorDemo\Calculators;

use CalculatorDemo\Parameters\Interfaces\ParameterContainerInterface;

final class SkullCalculator extends BaseCalculator
{
    public function __construct(ParameterContainerInterface $params)
    {
        parent::__construct($params);
    }

    /**
     * Concatenates two strings together, separated by a ':'
     * @return string
     */
    public function calculate():string
    {
        return $this->getParams()->getParameter(1)->getValue() .
            ' : ' .
            $this->getParams()->getParameter(2)->getValue();
    }
}