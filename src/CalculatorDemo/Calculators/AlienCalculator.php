<?php
/**
 * Created by PhpStorm.
 * User: Andrew Chapman
 */

namespace CalculatorDemo\Calculators;

use CalculatorDemo\Parameters\Interfaces\ParameterContainerInterface;

final class AlienCalculator extends BaseCalculator
{
    public function __construct(ParameterContainerInterface $params)
    {
        parent::__construct($params);
    }

    /**
     * Multiplies two integers together and returns the result.
     * @return int
     */
    public function calculate():int
    {
        return $this->getParams()->getParameter(1)->getValue() *
            $this->getParams()->getParameter(2)->getValue();
    }
}