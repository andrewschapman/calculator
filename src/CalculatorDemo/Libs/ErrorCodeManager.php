<?php
namespace CalculatorDemo\Libs;

class ErrorCodeManager
{
    private $errorCodes;

    public function __construct($errorCodes)
    {
        $this->errorCodes = $errorCodes;
    }

    public function getErrorCodeResponse($errorNumber)
    {
        if (isset($this->errorCodes[$errorNumber])) {
            return [[
                'code' => $errorNumber,
                'message' => $this->errorCodes[$errorNumber]['message']
            ], $this->errorCodes[$errorNumber]['httpStatusCode']];
        } else {
            return [[
                'code' => 0,
                'message' => 'An unknown error occurred - ' . $errorNumber
            ], 500];
        }
    }
}