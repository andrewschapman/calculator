<?php
define('ERRORCODE_GENERAL_EXCEPTION', 0);
define('ERRORCODE_INVALID_PARAMETERS', 1);
define('ERRORCODE_INVALID_PARAMETER', 2);

return [
    ERRORCODE_GENERAL_EXCEPTION => [
        'message' => 'An application error occurred',
        'httpStatusCode' => 500,
    ],
    ERRORCODE_INVALID_PARAMETERS => [
        'message' => 'Invalid parameters.  Please provide operator, parameter1 and parameter2',
        'httpStatusCode' => 400,
    ],
    ERRORCODE_INVALID_PARAMETER => [
        'message' => 'Invalid value for parameter',
        'httpStatusCode' => 400,
    ]
];
