/**
 * Created by andrewchapman on 28/02/2017.
 */
var Calculator;

(function($)
{
    "use strict";

    Calculator = function(formSelector) {
        this.formSelector = formSelector;

        this.init = function()
        {
            this.hideAllFeedback();

            $(this.formSelector).submit(function() {
                if(!this.validateForm()) {
                    return false;
                }

                this.sendData();

                return false;
            }.bind(this));
        };

        this.hideAllFeedback = function() {
            $('p#loading').hide();
            $('p#result').hide();
            $('p#error').hide();
        }

        this.showLoading = function() {
            this.hideAllFeedback();
            $('#btnSubmit').hide();

            $('p#loading').show();
        }

        this.loadingFinished = function() {
            $('#btnSubmit').show();
            $('p#loading').hide();
        }

        this.showResult = function(msg) {
            if (this.isScream()) {
                msg = this.convertUnicode(msg);
            }

            $('p#result').html('Result was: ' + msg);
            $('p#result').show();
        }

        this.convertUnicode = function(msg) {
            // Convert unicode to hex
            msg = msg.replace(/U\+1F383/ig, '&#127875;');
            msg = msg.replace(/U\+1F47E/ig, '&#128126;');
            msg = msg.replace(/U\+1F4A9/ig, '&#128169;');

            // Remove commas
            msg = msg.replace(/,/ig, '');
            msg = msg.replace(/ /ig, ' ---- ');

            return msg;
        }

        this.showError = function(msg) {
            $('p#error').text('Error: ' + msg);
            $('p#error').show();
        }

        this.isScream = function() {
            var operation = $(this.formSelector).find('#operation').val();
            return (operation == 'U+1F631');
        }

        this.sendData = function() {
            this.showLoading();

            var operation = $(this.formSelector).find('#operation').val();
            var parameter1 = $(this.formSelector).find('#parameter1').val();
            var parameter2 = $(this.formSelector).find('#parameter2').val();

            var url = '/index.php/calculator/' + operation + '/' + parameter1 + '/' + parameter2;

            $.get(url, {}, function(response) {
                this.loadingFinished();
                this.showResult(response.result);

            }.bind(this), 'JSON').fail(function(response) {
                this.loadingFinished();

                var unknownError = true;

                if(response.hasOwnProperty('responseJSON')) {
                    if(response.responseJSON.hasOwnProperty('code')) {
                        unknownError = false;

                        this.showError(
                            'Code ' + response.responseJSON.code
                            + ', '
                            + response.responseJSON.message
                            + ', '
                            + response.responseJSON.information
                        );
                    }
                }

                if(unknownError) {
                    alert('Sorry, your request failed. Please try again later');
                }
            }.bind(this));
        }

        this.validateForm = function() {
            if (!this.validateField($(this.formSelector).find('#parameter1'), 'Parameter 1')) {
                return false;
            }

            if (!this.validateField($(this.formSelector).find('#parameter2'), 'Parameter 2')) {
                return false;
            }

            return true;
        }

        this.validateField = function(fieldElement, fieldName) {
            if($(fieldElement).val() == '') {
                alert('Please enter ' + fieldName);
                $(fieldElement).focus();
                return false;
            }

            return true;
        }
    }

})(window.jQuery);